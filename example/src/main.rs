// Copyright 2023 hjiayz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
use anyhow::Result;
use std::{process::Stdio, str::FromStr, sync::Arc};
pub mod data;
pub mod grpcserver;
pub use grpcserver::routeguide;
use tonic::transport::Server;

use crate::routeguide::route_guide_server::{RouteGuide, RouteGuideServer};
use grpcserver::RouteGuideService;

//don't change name . mod path is crate::server
pub mod server {
    include!(concat!(env!("OUT_DIR"), "/server.rs"));
}
#[tokio::main]
pub async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:10000".parse().unwrap();

    println!("RouteGuideServer listening on: {}", addr);

    let route_guide: RouteGuideService = RouteGuideService {
        features: Arc::new(data::load()),
    };

    let svc = RouteGuideServer::new(route_guide);
    let t1 = tokio::spawn(async move {
        tonic::transport::Server::builder()
            .add_service(svc)
            .serve(addr)
            .await
    });
    let t1 = tokio::spawn(async move {
        server::init("http://localhost:10000").await.unwrap();
        let app = axum::Router::new()
            .route("/ws", axum::routing::get(server::handler))
            .route(
                "/rpc.js",
                axum::routing::get(|| async { (axum::TypedHeader(axum::headers::ContentType::from(mime::APPLICATION_JAVASCRIPT_UTF_8)),include_str!(concat!(env!("OUT_DIR"), "/rpc.js"))) }),
            )            
            .route(
                "/rpc.ts",
                axum::routing::get(|| async { (axum::TypedHeader(axum::headers::ContentType::from(mime::TEXT_PLAIN)),include_str!(concat!(env!("OUT_DIR"), "/rpc.ts"))) }),
            )
            .route(
                "/swserver.js",
                axum::routing::get(|| async { (axum::TypedHeader(axum::headers::ContentType::from(mime::APPLICATION_JAVASCRIPT_UTF_8)),include_str!(concat!(env!("OUT_DIR"), "/swserver.js"))) }),
            )
            .route(
                "/sw.js",
                axum::routing::get(|| async { (axum::TypedHeader(axum::headers::ContentType::from(mime::APPLICATION_JAVASCRIPT_UTF_8)),axum::TypedHeader(axum::headers::CacheControl::new().with_no_store()),include_str!("sw.js")) }),
            )
            .route(
                "/",
                axum::routing::get(|| async { axum::response::Html(include_str!("index.html")) }),
            );
        let saddr = "[::1]:8080";
        let addr = std::net::SocketAddr::from_str(saddr).unwrap();
        println!("webserver listening on http://localhost:8080/");
        axum::Server::bind(&addr)
            .serve(app.into_make_service())
            .await
            .unwrap();
    });
    t1.await?;
    Ok(())
}
