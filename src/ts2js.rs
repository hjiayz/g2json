// Copyright 2023 hjiay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::path::Path;

use deno_ast::swc::parser::{Syntax, TsConfig};

pub fn gen_js(path: &Path) -> Result<(), anyhow::Error> {
    let path = path.to_string_lossy();
    let (_code, _output, _error) =
        run_script::run_script!(format!("tsc --target es2020 --module es2020 {path}"))?;
    Ok(())
}

pub fn gen_js_deno(path: &Path) -> Result<(),anyhow::Error> {
    use deno_ast::parse_module;
    use deno_ast::MediaType;
    use deno_ast::ParseParams;
    use deno_ast::SourceTextInfo;
    use deno_ast::EmitOptions;
    let source_text = std::fs::read_to_string(path)?;
    let text_info = SourceTextInfo::new(source_text.into());
    let parsed_source = parse_module(ParseParams {
        specifier: "file:///rpc.ts".to_string(),
        media_type: MediaType::TypeScript,
        text_info,
        capture_tokens: true,
        maybe_syntax: None,
        scope_analysis: true,
      })?;
    let js = parsed_source.transpile(&EmitOptions::default())?;
    let mut newpath = path.to_path_buf();
    newpath.set_extension("js");
    std::fs::write(&newpath, js.text)?;
    if let Some(sourcemap) = js.source_map {
        newpath.set_extension("js.map");
        std::fs::write(newpath, sourcemap)?;
    }

    Ok(())
      
}
