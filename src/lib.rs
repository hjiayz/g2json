// Copyright 2023 hjiay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use anyhow::{Error, Result};
use deno_ast::swc::ast::TsKeywordType;
use heck::{AsSnakeCase, ToUpperCamelCase};
use std::collections::HashMap;
use std::convert::AsRef;
use std::hash::Hash;
use std::path::{Path, Display};
use prost::Message;

pub mod client;
pub mod ts2js;

mod graph;

const SUPER : &'static str = "crate::server";

pub fn parse(
    includes: &[impl AsRef<Path>],
    inputs: &[impl AsRef<Path>],
) -> Result<()> {
    tonic_build::configure()
    .file_descriptor_set_path(
        std::path::PathBuf::from(std::env::var("OUT_DIR").expect("OUT_DIR environment variable not set"))
            .join("file_descriptor_set.bin"))
    //.type_attribute(".", "#[derive(Hash)]")
    .compile(
        inputs,
        includes,
    ).map_err(|e|Error::msg("compile failed"))?;
    let file_descriptor_set_bytes = std::fs::read(format!("{}/file_descriptor_set.bin", std::env::var("OUT_DIR")?)).map_err(|e|Error::msg("read fd set failed"))?;
    let fd = prost_types::FileDescriptorSet::decode(&file_descriptor_set_bytes[..]).map_err(|e|Error::msg("fd decode failed"))?;
    let rust_code: String = gen_rust(&fd)?;

    let mut out_dir = std::path::PathBuf::from(std::env::var("OUT_DIR")?);
    out_dir.push("server.rs");
    std::fs::write(out_dir, rust_code).map_err(|e|Error::msg("write server.rs failed"))?;

    let ts_code: String = client::gen_ts(&fd)?;
    let mut out_dir = std::path::PathBuf::from(std::env::var("OUT_DIR")?);
    out_dir.push("rpc.ts");
    std::fs::write(&out_dir, ts_code).map_err(|e|Error::msg("write rpc.ts failed"))?;
    ts2js::gen_js_deno(&out_dir).map_err(|e|Error::msg("build rpc.js failed"))?;

    let swserver_code = include_str!("swserver.js");
    let mut out_dir = std::path::PathBuf::from(std::env::var("OUT_DIR")?);
    out_dir.push("swserver.js");
    std::fs::write(&out_dir, swserver_code).map_err(|e|Error::msg("write swserver.js failed"))?;
    Ok(())
}

fn gen_rust(fd: &prost_types::FileDescriptorSet) -> Result<String> {
    use graph::MessageGraph;
    let message_graph = MessageGraph::new(fd.file.iter())
    .map_err(|error| std::io::Error::new(std::io::ErrorKind::InvalidInput, error))?;
    let mut files = Vec::with_capacity(fd.file.len());
    let mut main = vec![];
    let mut msgnametopath = HashMap::new();
    for file in &fd.file {
        
        files.push(gen_file(file, &mut main,&message_graph,&mut msgnametopath)?);
    }
    
    let result = files.join("\n");
    let main = main.join("\n");
    Ok(format!(
        r#"


{result}

static mut ROUTES : Option<std::collections::HashMap::<&'static str,fn(&str,&str,tx:tokio::sync::mpsc::UnboundedSender<axum::extract::ws::Message>,&mut std::collections::HashMap<String,tokio::sync::mpsc::UnboundedSender<String>>,Option<&str>)>> = None;

pub async fn init(grpcurl:&str) -> Result<(),anyhow::Error> {{

    unsafe {{
        ROUTES = Some(std::collections::HashMap::<&'static str,fn(&str,&str,tx:tokio::sync::mpsc::UnboundedSender<axum::extract::ws::Message>,&mut std::collections::HashMap<String,tokio::sync::mpsc::UnboundedSender<String>>,Option<&str>)>::new());
    }}
    {main}
    Ok(())
}}

pub async fn handler(ws: axum::extract::ws::WebSocketUpgrade) -> axum::response::Response {{
    fn handle(header:g2jsonrt::Header,body:&str,tx:tokio::sync::mpsc::UnboundedSender<axum::extract::ws::Message>,ups:&mut std::collections::HashMap<String,tokio::sync::mpsc::UnboundedSender<String>>){{
        if let Some(route) = &unsafe {{ROUTES.as_ref().unwrap().get(header.path)}} {{
            route(header.id,body,tx,ups,header.status);
        }}
        else {{
            let message = g2jsonrt::make_return_message::<()>(header.id,Some("route mismatch"),None);
            tx.send(message);
        }}
    }}
    ws.on_upgrade(|socket| g2jsonrt::onhander(socket, handle))
}}
"#
    ))
}

fn gen_file(f: &prost_types::FileDescriptorProto, main: &mut Vec<String>,message_graph:&graph::MessageGraph,
    msgnametopath:&mut HashMap<String,(String,String)>) -> Result<String> {
    let name = f.package();
    let syntax = f.syntax();
    let mut messages = Vec::with_capacity(f.message_type.len());
    for msg in &f.message_type {

        messages.push(gen_message(msg, name, syntax,message_graph,msgnametopath,None)?);

    }
    let messages = messages.join("\n");

    let mut enums = Vec::with_capacity(f.enum_type.len());
    for tenum in &f.enum_type {

        enums.push(gen_enum(tenum)?);
    }
    let enums = enums.join("\n");

    let mut services = Vec::with_capacity(f.service.len());
    for service in &f.service {

        services.push(gen_service(name, service)?);

    }

    let services = services.join("\n");
    let sep = if name.is_empty() {""} else {"::"};

    main.push(format!(
        r#"
    unsafe {{
        {name}{sep}routes(ROUTES.as_mut().unwrap());
    }}
    unsafe {{
        {name}{sep}CON = Some(tonic::transport::Endpoint::from_shared(grpcurl.to_string())?
            .connect()
            .await?);
    }}
    
    "#
    
    ));
    if name.is_empty() {
        return Ok(format!(
            r#"
    {messages}
    {enums}
    {services}
        pub mod pb {{
            tonic::include_proto!("_");
        }}
        pub static mut CON : Option<tonic::transport::Channel> = None;
        "#
        ));
    }
    Ok(format!(
        r#"
    pub mod {name} {{
{messages}
{enums}
{services}
        pub mod pb {{
            tonic::include_proto!("{name}");
        }}
        pub static mut CON : Option<tonic::transport::Channel> = None;
    }}
    "#
    ))
}

fn gen_message(msg: &prost_types::DescriptorProto, package: &str, syntex: &str,message_graph:&graph::MessageGraph,
    msgnametopath:&mut HashMap<String,(String,String)>,parent:Option<&str>) -> Result<String> {
    let name = msg.name();

    let fq_message_name = match parent {
        Some(s)=>format!("{s}.{name}"),
        None => format!(
        "{}{}.{}",
        if package.is_empty() { "" } else { "." },
        package,
        name),
    };
    if let Some(opt) = &msg.options {
        if opt.map_entry() {
            return get_map_type(msg,&fq_message_name,msgnametopath).map(|_|"".to_string());
        }
        
    }

    let mut nest_messages: String = String::new();
    for nest in &msg.nested_type {
        nest_messages.push_str(&gen_message(nest,package,syntex,message_graph,msgnametopath,Some(&fq_message_name))?);
    }
    let mut fields = Vec::with_capacity(msg.field.len());
    let mut froms = Vec::new();
    let mut intos = Vec::new();
    for field in &msg.field {
        if field.oneof_index.is_some() {
            continue;
        }
        fields.push(gen_message_field(field, &mut froms, &mut intos, syntex,message_graph,&fq_message_name,msgnametopath)?);
    }
    
    gen_oneof(msg,package,syntex,message_graph,msgnametopath,Some(&fq_message_name),&mut nest_messages,&mut fields,&mut froms, &mut intos)?;

    let fields = fields.join("");
    let froms = froms.join("");
    let intos = intos.join("");

    let sname = AsSnakeCase(name);
    let pbtypename = pbtype(&fq_message_name, package);
    Ok(format!(
        r#"
        pub mod {sname}{{{nest_messages}

        }}

        #[serde_with::serde_as]
        #[derive(serde::Deserialize,serde::Serialize)]
        pub struct {name} {{{fields}
        }}

        impl From<{pbtypename}> for {name} {{
            fn from(src:{pbtypename}) -> Self {{
                {name} {{
{froms}
                }}
            }}
        }}
        impl Into<{pbtypename}> for {name} {{
            fn into(self) -> {pbtypename} {{
                {pbtypename} {{
{intos}
                }}
            }}
        }}
        "#
    ))
}

fn gen_message_field(
    field: &prost_types::FieldDescriptorProto,
    froms: &mut Vec<String>,
    intos: &mut Vec<String>,
    syntex: &str,
    message_graph:&graph::MessageGraph,
    fq_message_name:&str,
    msgnametopath:&mut HashMap<String,(String,String)>,
) -> Result<String> {
    use prost_types::field_descriptor_proto::{Type,Label};
    let name = field.name();
    let mut is_scalar = true;
    let mut attrs = String::new();
    let mut boxed = false;
    let mut ty;
    (ty,attrs,is_scalar) = gettype(field, msgnametopath)?;
    if !is_scalar {
        boxed = message_graph.is_nested(field.type_name(), fq_message_name);
        if boxed {
            ty = format!("Box<{}>",ty);
        }
    }
    let intoboxed = |inner:&str|{
        if boxed {
            format!("Box::new((*{}).into())",inner)
        }
        else {
            format!("{}.into()",inner)
        }
    };
    match field.label() {
        Label::Optional => {
            attrs = SerdeAs(&attrs).to_string();
            if syntex == "proto2" || (!is_scalar) {
                
                froms.push(format!(
                    "                {name}:src.{name}.map(|value|{}),\n"
                ,intoboxed("value")));
                intos.push(format!(
                    "                {name}:self.{name}.map(|value|{}),\n"
                    ,intoboxed("value")));
                Ok(format!(
                    r#"{attrs}
                    {name} : Option<{ty}>,"#
                ))
            } else {
                froms.push(format!("                {name}:{},\n",intoboxed(&format!("src.{name}"))));
                intos.push(format!("                {name}:{},\n",intoboxed(&format!("self.{name}"))));
                Ok(format!(
                    r#"{attrs}
                    {name} : {ty},"#
                ))
            }
        }
        Label::Required => {
            attrs = SerdeAs(&attrs).to_string();
            froms.push(format!("                {name}:{},\n",intoboxed(&format!("src.{name}"))));
            intos.push(format!("                {name}:{},\n",intoboxed(&format!("self.{name}"))));
            Ok(format!(
                r#"{attrs}
                    {name} : {ty},"#
            ))
        }
        Label::Repeated => {
            if ty.starts_with("std::collections::HashMap<") {
                attrs = SerdeAs(&attrs).to_string();
                froms.push(format!("                {name}:{},\n",&format!("src.{name}.into()")));
                intos.push(format!("                {name}:{},\n",&format!("self.{name}.into()")));
                return             Ok(format!(
                    r#"{attrs}
                        {name} : {ty},"#
                ))
            }
            attrs = AsVec(&attrs).to_string();
            attrs = SerdeAs(&attrs).to_string();
            froms.push(format!("                {name}:{},\n",intoboxed(&format!("src.{name}"))));
            intos.push(format!("                {name}:{},\n",intoboxed(&format!("self.{name}"))));
            Ok(format!(
                r#"{attrs}
                    {name} : Vec<{ty}>,"#
            ))
        }
    }
}

fn gen_enum(tenum: &prost_types::EnumDescriptorProto) -> Result<String> {
    let name = tenum.name();
    let mut values = Vec::with_capacity(tenum.value.len());
    for value in &tenum.value {
        values.push(gen_enum_value(value)?);
    }
    let values = values.join("");
    Ok(format!(
        r#"
        enum {name} {{{values}
        }}
        "#
    ))
}

fn gen_enum_value(value: &prost_types::EnumValueDescriptorProto) -> Result<String> {
    let name = value.name();
    let id = value.number();

    Ok(format!(
        r#"
            {name} = {id},"#
    ))
}

fn gen_service(packagename: &str, service: &prost_types::ServiceDescriptorProto) -> Result<String> {
    let servicename = service.name();
    let name = format!("{}{packagename}/{servicename}",if packagename.is_empty() {""} else {"/"});
    let mut methods = Vec::with_capacity(service.method.len());
    for method in &service.method {
        methods.push(gen_method(&name,servicename,packagename, method)?);
    }
    let methods = methods.join("");
    Ok(format!(
        r#"
    pub fn routes(routelist : &mut std::collections::HashMap::<&'static str,fn(&str,&str,tx:tokio::sync::mpsc::UnboundedSender<axum::extract::ws::Message>,&mut std::collections::HashMap<String,tokio::sync::mpsc::UnboundedSender<String>>,Option<&str>)> ) {{
        {methods}
    }}
    
    "#
    ))
}

fn gen_method(name: &str,servicename:&str,packagename: &str, method: &prost_types::MethodDescriptorProto) -> Result<String> {
    let method_name = method.name();
    let path = format!("{name}/{method_name}");
    
    let f = match (method.server_streaming(), method.client_streaming()) {
        (true, true) => gen_bidistream(method_name,servicename,packagename, method,&path)?,
        (false, true) => gen_upstream(method_name,servicename,packagename, method,&path)?,
        (true, false) => gen_downstream(method_name,servicename,packagename, method,&path)?,
        (false, false) => gen_once(method_name,servicename,packagename, method,&path)?,
    };
    Ok(format!(
        r#"
{f}
        routelist.insert("{path}",{method_name} as fn(&str,&str,tx:tokio::sync::mpsc::UnboundedSender<axum::extract::ws::Message>,&mut std::collections::HashMap<String,tokio::sync::mpsc::UnboundedSender<String>>,Option<&str> ));
"#
    ))
}

fn gen_bidistream(name: &str,servicename: &str,packagename: &str, method: &prost_types::MethodDescriptorProto,path:&str) -> Result<String> {
    
    let input_type = rtype(method.input_type());
    let pbinput_type = pbtype(method.input_type(), packagename);
    let output_type = rtype(method.output_type());
    let sname = heck::AsSnakeCase(name);
    let sservicename = heck::AsSnakeCase(servicename);
    let package_path = format!("{}{}",if packagename.is_empty() {""} else {"::"},packagename);
    Ok(format!(
        r#"

        #[allow(non_snake_case)]
        fn {name}(id:&str,body:&str,tx:tokio::sync::mpsc::UnboundedSender<axum::extract::ws::Message>,ups:&mut std::collections::HashMap<String,tokio::sync::mpsc::UnboundedSender<String>>,status:Option<&str>) {{
            match ups.get(id) {{
                Some(tx2)=>{{
                    if status==Some("close") {{
                        ups.remove(id);
                        return;
                    }}
                    if let Err(e) = tx2.send(body.to_string()) {{
                        let message = g2jsonrt::make_return_message::<()>(id,Some(&format!("stream closed:{{}}",e.to_string())),None);
                        tx.send(message);
                    }}
                    return;
                }}
                None=>(),
            }};
            let (tx2,rx2) = tokio::sync::mpsc::unbounded_channel::<String>();
            let tx3 = tx.clone();
            let return_id : std::sync::Arc<str> = std::sync::Arc::from(id);
            tokio::spawn(async move {{
                use tokio_stream::StreamExt;
                let tx4 = tx.clone();
                let return_id2 = return_id.clone();
                let return_id3 = return_id.clone();
                let stream = tokio_stream::wrappers::UnboundedReceiverStream::new(rx2).map_while(move |body|{{
                    let payload : {input_type} = match serde_json::from_str(&body) {{
                        Ok(val)=>val,
                        Err(e) => {{
                            let message = g2jsonrt::make_return_message::<()>(&return_id3,Some(&format!("serde parse failed:{{}}",e.to_string())),None);
                            tx4.send(message);
                            return None;
                        }},
                    }};
                    Some({pbinput_type}::from(payload.into()))
                }});
                let request = tonic::Request::new(stream);
                let mut client = {SUPER}{package_path}::pb::{sservicename}_client::{servicename}Client::new(unsafe {{CON.clone().unwrap()}});
                let res = match client.{sname}(request).await{{
                    Ok(res)=>{{
                        let mut inner = res.into_inner();
                        loop {{
                            match inner.message().await {{
                                Ok(Some(msg))=>{{
                                    let res = {output_type}::from(msg);
                                    let message = g2jsonrt::make_return_message(&return_id,None,Some(res));
                                    tx.send(message);
                                }},
                                Ok(None)=>{{
                                    let message = g2jsonrt::make_return_message::<()>(&return_id,Some("close"),None);
                                    tx.send(message);
                                    break;
                                }},
                                Err(e)=>{{
                                    let message = g2jsonrt::make_return_message::<()>(&return_id,Some(&format!("error from grpc:{{}}",e.message())),None);
                                    tx.send(message);
                                    break;
                                }}
                            }}
                        }}
                    }},
                    Err(e)=>{{
                        let message = g2jsonrt::make_return_message::<()>(&return_id2,Some(&format!("grpc request failed:{{}}",e.message())),None);
                        tx.send(message);
                    }}
                }};
            }});
            if let Err(e) = tx2.send(body.to_string()) {{
                let message = g2jsonrt::make_return_message::<()>(id,Some(&format!("stream closed:{{}}",e.to_string())),None);
                tx3.send(message);
            }}
            else {{
                ups.insert(id.to_string(),tx2);
            }}
        }}
    
    "#
    ))
}
fn gen_upstream(name: &str,servicename: &str,packagename: &str, method: &prost_types::MethodDescriptorProto,path:&str) -> Result<String> {
    
    let input_type = rtype(method.input_type());
    let pbinput_type = pbtype(method.input_type(), packagename);
    let output_type = rtype(method.output_type());
    let sname = heck::AsSnakeCase(name);
    let sservicename = heck::AsSnakeCase(servicename);
    let package_path = format!("{}{}",if packagename.is_empty() {""} else {"::"},packagename);
    Ok(format!(
        r#"

        #[allow(non_snake_case)]
        fn {name}(id:&str,body:&str,tx:tokio::sync::mpsc::UnboundedSender<axum::extract::ws::Message>,ups:&mut std::collections::HashMap<String,tokio::sync::mpsc::UnboundedSender<String>>,status:Option<&str>) {{
            match ups.get(id) {{
                Some(tx2)=>{{
                    if status==Some("close") {{
                        ups.remove(id);
                        return;
                    }}
                    if let Err(e) = tx2.send(body.to_string()) {{
                        let message = g2jsonrt::make_return_message::<()>(id,Some(&format!("stream closed:{{}}",e.to_string())),None);
                    }}
                    return;
                }}
                None=>(),
            }};
            let (tx2,rx2) = tokio::sync::mpsc::unbounded_channel::<String>();
            let tx3 = tx.clone();
            let return_id : std::sync::Arc<str> = std::sync::Arc::from(id);
            tokio::spawn(async move {{
                use tokio_stream::StreamExt;
                let tx4 = tx.clone();
                let return_id2 = return_id.clone();
                let stream = tokio_stream::wrappers::UnboundedReceiverStream::new(rx2).map_while(move |body|{{
                    let payload : {input_type} = match serde_json::from_str(&body) {{
                        Ok(val)=>val,
                        Err(e) => {{
                            let message = g2jsonrt::make_return_message::<()>(&return_id,Some(&format!("serde parse failed:{{}}",e.to_string())),None);
                            tx4.send(message);
                            return None;
                        }},
                    }};
                    Some({pbinput_type}::from(payload.into()))
                }});
                let request = tonic::Request::new(stream);
                let mut client = {SUPER}{package_path}::pb::{sservicename}_client::{servicename}Client::new(unsafe {{CON.clone().unwrap()}});
                let res = match client.{sname}(request).await{{
                    Ok(res)=>{{
                        let res = {output_type}::from(res.into_inner());
                        let message = g2jsonrt::make_return_message(&return_id2,None,Some(res));
                        tx.send(message);
                    }},
                    Err(e)=>{{
                        let message = g2jsonrt::make_return_message::<()>(&return_id2,Some(&format!("client request failed:{{}}",e.message())),None);
                        tx.send(message);
                    }}
                }};
            }});
            if let Err(e) = tx2.send(body.to_string()) {{
                let message = g2jsonrt::make_return_message::<()>(id,Some(&e.to_string()),None);
                tx3.send(message);
            }}
            else {{
                ups.insert(id.to_string(),tx2);
            }}
        }}
    
    "#
    ))
}
fn gen_downstream(name: &str,servicename: &str,packagename: &str, method: &prost_types::MethodDescriptorProto,path:&str) -> Result<String> {
    let input_type = rtype(method.input_type());
    let pbinput_type = pbtype(method.input_type(), packagename);
    let output_type = rtype(method.output_type());
    let sname = heck::AsSnakeCase(name);
    let sservicename = heck::AsSnakeCase(servicename);
    let package_path = format!("{}{}",if packagename.is_empty() {""} else {"::"},packagename);
    Ok(format!(
        r#"

        #[allow(non_snake_case)]
        fn {name}(id:&str,body:&str,tx:tokio::sync::mpsc::UnboundedSender<axum::extract::ws::Message>,ups:&mut std::collections::HashMap<String,tokio::sync::mpsc::UnboundedSender<String>>,status:Option<&str>) {{
            let payload : {input_type} = match serde_json::from_str(body) {{
                Ok(val)=>val,
                Err(e) => {{
                    let message = g2jsonrt::make_return_message::<()>(id,Some(&format!("serde parse failed:{{}}",e.to_string())),None);
                    tx.send(message);
                    return;
                }},
            }};
            let request = tonic::Request::new({pbinput_type}::from(payload.into()));
            let mut client = {SUPER}{package_path}::pb::{sservicename}_client::{servicename}Client::new(unsafe {{CON.clone().unwrap()}});
            let return_id : Box<str> = Box::from(id);
            tokio::spawn(async move {{
                let res = match client.{sname}(request).await{{
                    Ok(res)=>{{
                        let mut inner = res.into_inner();
                        loop {{
                            match inner.message().await {{
                                Ok(Some(msg))=>{{
                                    let res = {output_type}::from(msg);
                                    let message = g2jsonrt::make_return_message(&return_id,None,Some(res));
                                    tx.send(message);
                                }},
                                Ok(None)=>{{
                                    let message = g2jsonrt::make_return_message::<()>(&return_id,Some("close"),None);
                                    tx.send(message);
                                    break;
                                }},
                                Err(e)=>{{
                                    let message = g2jsonrt::make_return_message::<()>(&return_id,Some(&format!("client request failed:{{}}",e.message())),None);
                                    tx.send(message);
                                    break;
                                }}
                            }}
                        }}
                    }},
                    Err(e)=>{{
                        let message = g2jsonrt::make_return_message::<()>(&return_id,Some(&format!("client request failed:{{}}",e.message())),None);
                        tx.send(message);
                    }}
                }};
            }});
            
        }}
    
    "#
    ))
}
fn gen_once(name: &str,servicename: &str,packagename: &str, method: &prost_types::MethodDescriptorProto,path:&str) -> Result<String> {
    let input_type = rtype(method.input_type());
    let pbinput_type = pbtype(method.input_type(), packagename);
    let output_type = rtype(method.output_type());
    let sname = heck::AsSnakeCase(name);
    let sservicename = heck::AsSnakeCase(servicename);
    let package_path = format!("{}{}",if packagename.is_empty() {""} else {"::"},packagename);
    Ok(format!(
        r#"
    #[allow(non_snake_case)]
    fn {name}(id:&str,body:&str,tx:tokio::sync::mpsc::UnboundedSender<axum::extract::ws::Message>,ups:&mut std::collections::HashMap<String,tokio::sync::mpsc::UnboundedSender<String>>,status:Option<&str>) {{
        let payload : {input_type} = match serde_json::from_str(body) {{
            Ok(val)=>val,
            Err(e) => {{
                let message = g2jsonrt::make_return_message::<()>(id,Some(&format!("serde parse failed:{{}}",e.to_string())),None);
                tx.send(message);
                return;
            }},
        }};
        let request = tonic::Request::new({pbinput_type}::from(payload.into()));
        let mut client = {SUPER}{package_path}::pb::{sservicename}_client::{servicename}Client::new(unsafe {{CON.clone().unwrap()}});
        let return_id : Box<str> = Box::from(id);
        tokio::spawn(async move {{
            let res = match client.{sname}(request).await{{
                Ok(res)=>{{
                    let res = {output_type}::from(res.into_inner());
                    let message = g2jsonrt::make_return_message(&return_id,None,Some(res));
                    tx.send(message);
                }},
                Err(e)=>{{
                    let message = g2jsonrt::make_return_message::<()>(&return_id,Some(&format!("client request failed:{{}}",e.message())),None);
                    tx.send(message);
                }}
            }};
        }});
        
    }}
    
    "#
    ))
}


fn rtype(fq_message_name:&str)->String{
    let names:Vec<&str> = fq_message_name.split(".").collect();
    let prefix:Vec<String> = names[..names.len()-1].into_iter().map(|s|heck::AsSnakeCase(s).to_string()).collect();
    let prefix = prefix.join("::");
    let last = heck::AsUpperCamelCase(names[names.len()-1]);
    format!("{SUPER}{prefix}::{last}")
}

fn pbtype(fq_message_name:&str,package: &str)->String {
    let names:Vec<&str> = fq_message_name.split(".").collect();
    let start = if package.is_empty() {
        0
    }
    else {
        2
    };
    let mut prefix:Vec<String> = names[start..names.len()-1].into_iter().map(|s|heck::AsSnakeCase(s).to_string()).collect();
    let last = heck::AsUpperCamelCase(names[names.len()-1]);
    prefix.push(last.to_string());
    let prefix = prefix.join("::");
    let sep = if prefix.starts_with("::") {
        ""
    }
    else {
        "::"
    };
    if package.is_empty() {
        format!("{SUPER}::pb{prefix}")
    }
    else {
        format!("{SUPER}::{package}::pb{sep}{prefix}")
    }
}

fn gettype(field: &prost_types::FieldDescriptorProto,msgnametopath:&mut HashMap<String,(String,String)>)->Result<(String,String,bool),anyhow::Error>{
    use prost_types::field_descriptor_proto::{Type,Label};
    let name = field.name();
    let mut tyname;
    let mut is_scalar = true;
    let mut attrs = String::new();
    let ty = match field.r#type() {
        Type::Bool => "bool",
        Type::Bytes => {
            attrs.push_str("serde_with::hex::Hex");
            "Vec<u8>"
        },
        Type::Float => "f32",
        Type::Double => "f64",
        Type::Fixed32 | Type::Uint32 => {
            "u32"
        },
        Type::Fixed64 | Type::Uint64 => {
            attrs.push_str("serde_with::DisplayFromStr");
            "u64"
        },
        Type::Int32 | Type::Sfixed32 | Type::Sint32 => {
            "i32"
        },
        Type::Int64 | Type::Sfixed64 | Type::Sint64 => {
            attrs.push_str("serde_with::DisplayFromStr");
            "i64"
    },
        Type::String => "String",
        Type::Enum => {
            is_scalar = false;
            tyname = format!("super{}", field.type_name().replace(".", "::"));
            &tyname
        }
        Type::Message => {
            is_scalar = false;
            (tyname,attrs) = msgnametopath.get(field.type_name()).map(|v|v.to_owned()).unwrap_or_else(||(rtype(field.type_name()),"".to_string()));
            &tyname
        }
        Type::Group => return Err(Error::msg("group is deprecated")),
    };
    Ok((ty.to_string(),attrs,is_scalar))
}

fn get_map_type(msg: &prost_types::DescriptorProto, fq_message_name:&str,
    msgnametopath:&mut HashMap<String,(String,String)>)->Result<(),anyhow::Error>{
    let mut tkey=String::new()  ;
    let mut tvalue =String::new();
    let mut akey  =String::new();
    let mut avalue =String::new();
    for  field in &msg.field {
        if field.name()=="key" {
            (tkey,akey,_) = gettype(field,msgnametopath)?;
            if akey == "" {
                akey = "_".to_string();
            }
        }
        else if field.name()=="value" {
            (tvalue,avalue,_) = gettype(field,msgnametopath)?;
            if avalue == "" {
                avalue = "_".to_string();
            }
        }
    }
    msgnametopath.insert(fq_message_name.to_string(), (format!("std::collections::HashMap<{tkey},{tvalue}>"),format!("serde_with::Seq<({akey},{avalue})>")));
    Ok(())
}

fn gen_oneof(msg: &prost_types::DescriptorProto, package: &str, syntex: &str,message_graph:&graph::MessageGraph,
    msgnametopath:&mut HashMap<String,(String,String)>,parent:Option<&str>,nest_type:&mut String,fields:&mut Vec<String>,froms:&mut Vec<String>,intos:&mut Vec<String>) ->Result<(),anyhow::Error> {
    let mut oneofitems = vec![(String::new(),Vec::<String>::new(),Vec::<String>::new(),Vec::<String>::new(),String::new());msg.oneof_decl.len()];
    let id = 0;
    for oneof in &msg.oneof_decl {
        let name = oneof.name.as_ref().ok_or_else(||Error::msg("missing oneof name"))?.to_string();
        froms.push(format!("                {name}:{},\n",&format!("src.{name}.map(|value| value.into() )")));
        intos.push(format!("                {name}:{},\n",&format!("self.{name}.map(|value| value.into() )")));
        oneofitems[id].0 = name;
    }
    
    for field in &msg.field {
        if let Some(id) = field.oneof_index {
            let name = oneofitems[id as usize].0.to_string();
            let fq_message_name = match parent {
                Some(s)=>format!("{s}.{name}"),
                None=>unreachable!(),
            };
            oneofitems[id as usize].4 = fq_message_name.clone();
            let fieldcode = gen_oneof_field(field, &mut oneofitems[id as usize], syntex, message_graph, &fq_message_name, msgnametopath)?;
            oneofitems[id as usize].1.push(fieldcode);

        }
    }
    for (name,codes,froms,intos,fq_message_name) in oneofitems {
        let code = codes.join("\n");
        
        let froms = froms.join("");
        let intos = intos.join("");
        let pbtypename = pbtype(&fq_message_name, package);
        let uname = heck::AsUpperCamelCase(&name).to_string();
        fields.push(format!(
            r#"
                {name} : Option<{}>,"#
        ,rtype(&fq_message_name)));
        nest_type.push_str(&format!(r#"
        #[serde_with::serde_as]
        #[derive(serde::Deserialize,serde::Serialize)]
        #[serde(rename_all = "snake_case")]
        pub enum {uname} {{
{code}
        }}
        impl From<{pbtypename}> for {uname} {{
            fn from(src:{pbtypename}) -> Self {{
                use {pbtypename} as pb;
                use {uname} as my;
                match src {{
{froms}
                }}
            }}
        }}
        impl Into<{pbtypename}> for {uname} {{
            fn into(self) -> {pbtypename} {{
                use {pbtypename} as pb;
                use {uname} as my;
                match self {{
{intos}
                }}
            }}
        }}
        "#));
    }
    Ok(())
}

fn gen_oneof_field(
    field: &prost_types::FieldDescriptorProto,
    infos: &mut (String, Vec<String>, Vec<String>, Vec<String>, String),
    syntex: &str,
    message_graph:&graph::MessageGraph,
    fq_message_name:&str,
    msgnametopath:&mut HashMap<String,(String,String)>,
) -> Result<String> {
    use prost_types::field_descriptor_proto::{Type,Label};
    let name = heck::AsUpperCamelCase(field.name());
    let is_scalar;
    let mut attrs;
    let mut boxed = false;
    let mut ty;
    (ty,attrs,is_scalar) = gettype(field, msgnametopath)?;
    if !is_scalar {
        boxed = message_graph.is_nested(field.type_name(), fq_message_name);
        if boxed {
            ty = format!("Box<{}>",ty);
        }
    }
    let intoboxed = |inner:&str|{
        if boxed {
            format!("Box::new((*{}).into())",inner)
        }
        else {
            format!("{}.into()",inner)
        }
    };
    match field.label() {
        Label::Optional | Label::Required => {
            attrs = SerdeAs(&attrs).to_string();
            infos.2.push(format!("pb::{name}(src)=>my::{name}({}),\n",intoboxed(&format!("src"))));
            infos.3.push(format!("my::{name}(src)=>pb::{name}({}),\n",intoboxed(&format!("src"))));
            Ok(format!(
                r#"{attrs}
                        {name}({ty}),"#
            ))
        }
        Label::Repeated => {
            if ty.starts_with("std::collections::HashMap<") {
                attrs = SerdeAs(&attrs).to_string();
                infos.2.push(format!("pb::{name}(src)=>my::{name}({}),\n",&format!("src.into()")));
                infos.3.push(format!("my::{name}(src)=>pb::{name}({}),\n",&format!("src.into()")));
                return             Ok(format!(
                    r#"{attrs}
                        {name}({ty}),"#
                ))
            }
            attrs = AsVec(&attrs).to_string();
            attrs = SerdeAs(&attrs).to_string();
            infos.2.push(format!("pb::{name}(src)=>my::{name}({}),\n",intoboxed(&format!("src"))));
            infos.3.push(format!("my::{name}(src)=>pb::{name}({}),\n",intoboxed(&format!("src"))));
            Ok(format!(
                r#"{attrs}
                    {name}(Vec<{ty}>),"#
            ))
        }
    }
}


struct SerdeAs<'a>(&'a str);
impl<'a> std::fmt::Display for SerdeAs<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.0.is_empty() {
            return f.write_str("");
        }
        f.write_fmt(format_args!("                    #[serde_as(as = \"{}\")]",self.0))
    }
}
struct AsVec<'a>(&'a str);
impl<'a> std::fmt::Display for AsVec<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.0.is_empty() {
            return f.write_str("");
        }
        f.write_fmt(format_args!("Vec<{}>",self.0))
    }
}