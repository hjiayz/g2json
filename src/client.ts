"use strict";
// Copyright 2023 hjiay
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

type ReturnMessage = {
    id: string,
    status: undefined | string,
    msg: any | undefined,
}

class Client {
    url: string;
    id: bigint;
    onstatuschange: (isopen:boolean) => void;
    ws: WebSocket | undefined;
    onMessages: Record<number, (msg: ReturnMessage) => void>;
    connecting:boolean;
    constructor(url: string, onstatuschange?: (isopen:boolean) => void) {
        this.url = url;
        this.id = 0n;
        this.onstatuschange = (isopen:boolean) => {
            if (!isopen) {
                for (let id in this.onMessages) {
                    let onmsg = this.onMessages[id];
                    if (onmsg == undefined) {
                        continue;
                    }
                    onmsg({
                        id: id.toString(),
                        status: "close",
                        msg: undefined,
                    });
                }
                this.onMessages={};
                this.id=0n;
            }
            if (onstatuschange!=null) {
                onstatuschange(isopen);
            }
        }
    }
    async connect() {
        if (this.ws != undefined) {
            if (0 == this.ws.readyState) {
                throw new Error("ws Connecting");
            }
            else if (this.ws.readyState == 1) {
                throw new Error("ws Connected");
            }
            else if (2 == this.ws.readyState) {
                throw new Error("ws Closing");
            }
        }
        return await new Promise((res, err) => {
            this.ws = new WebSocket(this.url);
            this.onMessages = {};
            let timeout = setTimeout(() => {
                this.ws?.close();
            }, 10000);
            this.ws.addEventListener('message', (event) => {
                let msg = event.data;
                let pack = JSON.parse(msg);
                if (typeof this.onMessages[pack.id] == "function") {
                    this.onMessages[pack.id](pack);
                }
                else {
                    console.error("bad resp:" + msg);
                }
            });
            this.ws.addEventListener('open', () => {
                clearInterval(timeout)
                this.onstatuschange(true);
                res(this)
            });
            this.ws.addEventListener('error', (e) => {
                /**
                 * @todo remove it
                 * 
                 */
                console.error("error : websocket error event", e);
                //err("connect failed");
            });
            this.ws.addEventListener('close', ()=>{
                this.onstatuschange(false)
                console.log("connect closed.reconnecting")
                setTimeout(() => {
                    this.connect().then(()=>{console.log("reconnected ok")});
                }, 2000);
            });
        })
    }
    getid() {
        if (this.ws === undefined) {
            throw new Error("please connect first");
        }
        this.id = this.id + 1n;
        return this.id;
    }
    call<In, Out>(path: string, data, encode: (p: In) => any, decode: (p: any) => Out) {
        let id = this.getid();
        return new Promise((res, err) => {
            try {
                this.onMessages[id.toString()] = (val: ReturnMessage): void => {
                    delete this.onMessages[id.toString()];
                    if (typeof val.status == "string") {
                        err(new Error(val.status));
                    }
                    else {
                        res(decode(val.msg));
                    }
                    return undefined;
                }
                if (this.ws === undefined) {
                    throw new Error("please connect first");
                }
                if (this.ws.readyState != WebSocket.OPEN) {
                    throw new Error("websocket readyState not OPEN");
                }
                this.ws.send(`${JSON.stringify({ path, id: id.toString() })}\n${JSON.stringify(encode(data))}`);
            }
            catch (e) {
                err(e);
            }
        });
    }
    upstream<In, Out>(path: string, encode: (p: In) => any, decode: (p: any) => Out, sender: Sender<In>) {
        return new Promise((res, err) => {
            let id = this.getid();
            if (this.ws === undefined) {
                throw new Error("please connect first");
            }
            sender.init(id.toString(), path, this.ws, encode);
            this.onMessages[id.toString()] = (val: ReturnMessage) => {
                sender.closed=true;
                delete this.onMessages[id.toString()];
                if (typeof val.status == "string") {
                    err(new Error(val.status));
                }
                else {
                    res(decode(val.msg));
                }
            }
        });
    }
    downstream<In, Out>(path: string, data: In, encode: (p: In) => any, decode: (p: any) => Out) {
        let id = this.getid();

        if (this.ws === undefined) {
            throw new Error("please connect first");
        }
        if (this.ws.readyState != WebSocket.OPEN) {
            throw new Error("websocket readyState not OPEN");
        }
        this.ws.send(`${JSON.stringify({ path, id: id.toString() })}\n${JSON.stringify(encode(data))}`);
        let msgs: [Out | undefined, Error | undefined][] = [];
        let event: [((arg0: undefined) => void) | undefined] = [undefined];
        let res = (r: Out) => {
            msgs.push([r, undefined]);
            if (event[0] !== undefined) {
                let ev = event[0];
                event[0] = undefined;
                ev(undefined);
            }
        }
        let err = (e: Error) => {
            msgs.push([undefined, e]);
            if (event[0] !== undefined) {
                let ev = event[0];
                event[0] = undefined;
                ev(undefined);
            }
        }
        this.onMessages[id.toString()] = (val: ReturnMessage) => {
            if (typeof val.status == "string") {
                delete this.onMessages[id.toString()];
                err(new Error(val.status));
            }
            else {
                res(decode(val.msg));
            }
        }
        return {
            async*[Symbol.asyncIterator]() {
                while (true) {
                    let msg = msgs.shift();
                    if (msg !== undefined) {
                        if ((msg[1] !== undefined) && (msg[1].message == "close")) {
                            break;
                        }
                        if (msg[1] !== undefined) {
                            throw msg[1];
                            continue;
                        }
                        if (msg[0] !== undefined) {
                            yield msg[0];
                            continue;
                        }
                    }
                    await new Promise((res, err) => {
                        event[0] = res;
                    });
                }
            }
        };
    }
    bidistream<In, Out>(path: string, encode: (p: In) => any, decode: (p: any) => Out, sender: Sender<In>) {
        let id = this.getid();
        if (this.ws === undefined) {
            throw new Error("please connect first");
        }
        sender.init(id.toString(), path, this.ws, encode);
        let msgs: [Out | undefined, Error | undefined][] = [];
        let event: [((arg0: undefined) => void) | undefined] = [undefined];
        let res = (r: Out) => {
            msgs.push([r, undefined]);
            if (event[0] !== undefined) {
                let ev = event[0];
                event[0] = undefined;
                ev(undefined);
            }
        };
        let err = (e: Error) => {
            msgs.push([undefined, e]);
            if (event[0] !== undefined) {
                let ev = event[0];
                event[0] = undefined;
                ev(undefined);
            }
        }
        this.onMessages[id.toString()] = (val: ReturnMessage) => {
            if (typeof val.status == "string") {
                sender.closed=true;
                delete this.onMessages[id.toString()];
                err(new Error(val.status));
            }
            else {
                res(decode(val.msg));
            }
        }
        return {
            async*[Symbol.asyncIterator]() {
                while (true) {
                    let msg = msgs.shift();
                    if (msg !== undefined) {
                        if ((msg[1] !== undefined) && (msg[1].message == "close")) {
                            break;
                        }
                        if (msg[1] !== undefined) {
                            throw msg[1];
                            continue;
                        }
                        if (msg[0] !== undefined) {
                            yield msg[0];
                            continue;
                        }
                    }
                    await new Promise((res, err) => {
                        event[0] = res;
                    });
                }
            }
        };
    }
}

export class Sender<T>{
    id: string;
    path: string;
    ws: WebSocket;
    encode: (arg: T) => any;
    closed:boolean;
    constructor() {
    }
    init(id: string, path: string, ws: WebSocket, encode: (arg: T) => any) {
        this.id = id;
        this.path = path;
        this.ws = ws;
        this.encode = encode;
        this.closed = false;
    }
    send(data: T) {
        if (this.id == undefined) {
            throw new Error("Sender never init ");
        }
        if (this.closed) {
            throw new Error("sender closed");
        }
        if (this.ws.readyState != WebSocket.OPEN) {
            throw new Error("websocket readyState not OPEN");
        }
        this.ws.send(`${JSON.stringify({ path: this.path, id: this.id })}\n${JSON.stringify(this.encode(data))}`);
    }
    close() {
        if (this.id == undefined) {
            throw new Error("Sender never init ");
        }
        if (this.closed) {
            throw new Error("sender closed");
        }
        if (this.ws.readyState != WebSocket.OPEN) {
            throw new Error("websocket readyState not OPEN");
        }
        this.ws.send(`${JSON.stringify({ path: this.path, id: this.id, status: "close" })}\n`);
    }
}



class SWClient {
    url: string;
    id: bigint;
    onstatechange: (isopen:boolean) => void;
    onMessages: Record<number, (msg: ReturnMessage) => void>;
    ws: ServiceWorker;
    constructor(onstatechange?: (isopen:boolean) => void) {
        this.id = 0n;
        this.onstatechange = (isopen:boolean) => {
            for (let id in this.onMessages) {
                let onmsg = this.onMessages[id];
                if (onmsg == undefined) {
                    continue;
                }
                onmsg({
                    id: id.toString(),
                    status: "close",
                    msg: undefined,
                });
            }
            if (onstatechange != undefined) {
                onstatechange(isopen);
            }
        }
    }
    async connect() {
        if (this.ws != undefined) {
            if (this.ws.state !== "activated") {
                throw new Error("service worker state not activated");
            }
        }
        const registerServiceWorker = async () => {
            if ("serviceWorker" in navigator) {
                try {
                    const registration = await navigator.serviceWorker.register("/sw.js", {
                        scope: "/",
                        type: "module",
                    });
                    console.log(registration);
                    if (registration.active) {
                        console.log("sw active");
                        return registration.active;
                    }
                    else {
                        let newsw = (await navigator.serviceWorker.ready).active;
                        if (newsw === null) {
                            throw Error("service worker not active");
                        }
                        console.log("newsw active");
                        console.log(newsw.state);
                        return newsw;
                    }
                } catch (error) {
                    throw new Error(`Registration failed with ${error}`);
                }
            }
            throw new Error("register failed");
        };


        this.ws = await registerServiceWorker();
        let inited = false;
        return await new Promise((res, err) => {
            this.onMessages = {};
            navigator.serviceWorker.addEventListener('message', (event) => {
                let { ev, data } = event.data;
                if (ev == "close") {
                    this.onstatechange(false);
                } else if (ev == "open") {
                    this.onstatechange(true);
                    if (!inited) {
                        inited=true;
                        res(this);
                    }
                } else if (ev == "error") {
                    /**
                     * @todo remove it
                     * 
                     */
                    console.error("error : websocket error event", JSON.stringify(data));
                    //err("connect failed");
                } else if (ev == "msg") {
                    let pack = data;
                    if (typeof this.onMessages[pack.id] == "function") {
                        this.onMessages[pack.id](pack);
                    }
                    else {
                        console.error("bad resp:" + data);
                    }
                } else {
                    console.error("bug: unknown event name " + ev);
                }
            });
            navigator.serviceWorker.startMessages();
            this.ws.postMessage({
                ev: "isopen",
            });

        })
    }
    getid() {
        if (this.ws === undefined) {
            throw new Error("please connect first");
        }
        this.id = this.id + 1n;
        return this.id;
    }
    call<In, Out>(path: string, data, encode: (p: In) => any, decode: (p: any) => Out) {
        let id = this.getid();
        return new Promise((res, err) => {
            try {
                this.onMessages[id.toString()] = (val: ReturnMessage): void => {
                    delete this.onMessages[id.toString()];
                    if (typeof val.status == "string") {
                        err(new Error(val.status));
                    }
                    else {
                        res(decode(val.msg));
                    }
                    return undefined;
                }
                if (this.ws.state !== "activated") {
                    throw new Error("service worker not activated");
                }
                this.ws.postMessage({
                    ev: "send",
                    data: {
                        path: path,
                        id: id.toString(),
                        msg: JSON.stringify(encode(data)),
                    }
                });
            }
            catch (e) {
                err(e);
            }
        });
    }
    upstream<In, Out>(path: string, encode: (p: In) => any, decode: (p: any) => Out, sender: SWSender<In>) {
        return new Promise((res, err) => {
            let id = this.getid();
            if (this.ws === undefined) {
                throw new Error("please connect first");
            }
            sender.init(id.toString(), path, this.ws, encode);
            this.onMessages[id.toString()] = (val: ReturnMessage) => {
                delete this.onMessages[id.toString()];
                sender.closed=true;
                if (typeof val.status == "string") {
                    err(new Error(val.status));
                }
                else {
                    res(decode(val.msg));
                }
            }
        });
    }
    downstream<In, Out>(path: string, data: In, encode: (p: In) => any, decode: (p: any) => Out) {
        let id = this.getid();

        if (this.ws === undefined) {
            throw new Error("please connect first");
        }
        if (this.ws.state !== "activated") {
            throw new Error("service worker not activated");
        }
        this.ws.postMessage({
            ev: "send",
            data: {
                path: path,
                id: id.toString(),
                msg: JSON.stringify(encode(data)),
            }
        });
        let msgs: [Out | undefined, Error | undefined][] = [];
        let event: [((arg0: undefined) => void) | undefined] = [undefined];
        let res = (r: Out) => {
            msgs.push([r, undefined]);
            if (event[0] !== undefined) {
                let ev = event[0];
                event[0] = undefined;
                ev(undefined);
            }
        }
        let err = (e: Error) => {
            msgs.push([undefined, e]);
            if (event[0] !== undefined) {
                let ev = event[0];
                event[0] = undefined;
                ev(undefined);
            }
        }
        this.onMessages[id.toString()] = (val: ReturnMessage) => {
            if (typeof val.status == "string") {
                delete this.onMessages[id.toString()];
                err(new Error(val.status));
            }
            else {
                res(decode(val.msg));
            }
        }
        return {
            async*[Symbol.asyncIterator]() {
                while (true) {
                    let msg = msgs.shift();
                    if (msg !== undefined) {
                        if ((msg[1] !== undefined) && (msg[1].message == "close")) {
                            break;
                        }
                        if (msg[1] !== undefined) {
                            throw msg[1];
                            continue;
                        }
                        if (msg[0] !== undefined) {
                            yield msg[0];
                            continue;
                        }
                    }
                    await new Promise((res, err) => {
                        event[0] = res;
                    });
                }
            }
        };
    }
    bidistream<In, Out>(path: string, encode: (p: In) => any, decode: (p: any) => Out, sender: SWSender<In>) {
        let id = this.getid();
        if (this.ws === undefined) {
            throw new Error("please connect first");
        }
        sender.init(id.toString(), path, this.ws, encode);
        let msgs: [Out | undefined, Error | undefined][] = [];
        let event: [((arg0: undefined) => void) | undefined] = [undefined];
        let res = (r: Out) => {
            msgs.push([r, undefined]);
            if (event[0] !== undefined) {
                let ev = event[0];
                event[0] = undefined;
                ev(undefined);
            }
        };
        let err = (e: Error) => {
            msgs.push([undefined, e]);
            if (event[0] !== undefined) {
                let ev = event[0];
                event[0] = undefined;
                ev(undefined);
            }
        }
        this.onMessages[id.toString()] = (val: ReturnMessage) => {
            if (typeof val.status == "string") {
                delete this.onMessages[id.toString()];
                sender.closed=true;
                err(new Error(val.status));
            }
            else {
                res(decode(val.msg));
            }
        }
        return {
            async*[Symbol.asyncIterator]() {
                while (true) {
                    let msg = msgs.shift();
                    if (msg !== undefined) {
                        if ((msg[1] !== undefined) && (msg[1].message == "close")) {
                            break;
                        }
                        if (msg[1] !== undefined) {
                            throw msg[1];
                            continue;
                        }
                        if (msg[0] !== undefined) {
                            yield msg[0];
                            continue;
                        }
                    }
                    await new Promise((res, err) => {
                        event[0] = res;
                    });
                }
            }
        };
    }
}

export class SWSender<T>{
    id: string;
    path: string;
    ws: ServiceWorker;
    encode: (arg: T) => any;
    closed:boolean;
    constructor() {
    }
    init(id: string, path: string, ws: ServiceWorker, encode: (arg: T) => any) {
        this.id = id;
        this.path = path;
        this.encode = encode;
        this.ws = ws;
        this.closed = false;
    }
    send(data: T) {
        if (this.closed) {
            throw new Error("this sender closed");
        }
        if (this.ws.state !== "activated") {
            throw new Error("service worker not activated");
        }
        this.ws.postMessage({
            ev: "send",
            data: {
                path: this.path,
                id: this.id.toString(),
                msg: JSON.stringify(this.encode(data)),
            }
        });
    }
    close() {
        if (this.ws.state !== "activated") {
            throw new Error("service worker not activated");
        }
        this.ws.postMessage({
            ev: "send",
            data: {
                path: this.path,
                id: this.id.toString(),
                status: "close",
                msg: "",
            }
        });
    }
}