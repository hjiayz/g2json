// Copyright 2023 hjiay
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export class SWServer {
    constructor(url){
        if (typeof url!="string") {
            throw new Error("url not string");
        }
        const wsinit = (url)=>{
            this.ws = new WebSocket(url);
            this.ws.addEventListener('message', (event) => {
                (async()=>{
                    let msg = event.data;
                    let pack = JSON.parse(msg);
                    let [clientid,msgid] = pack.id.split(",");
                    pack.id = msgid;
                    const thisclient = await clients.get(clientid);
                    thisclient.postMessage({ev:"msg",data:pack});
                })()
            });
            let timeout = setTimeout(() => {
                this.ws.close();
            }, 10000);
            this.ws.addEventListener('open', async()=>{
                console.log("websocket opened");
                clearTimeout(timeout);
                const allClients = await clients.matchAll({
                    includeUncontrolled: true,
                });
                for (const client of allClients) {
                    client.postMessage({ev:"open"});
                }
            });
            this.ws.addEventListener('error', async(e)=>{
                const allClients = await clients.matchAll({
                    includeUncontrolled: false,
                });
                for (const client of allClients) {
                    client.postMessage({ev:"error",data:e.toString()});
                }
                this.ws.close();
            });
            this.ws.addEventListener('close', async()=>{
                const allClients = await clients.matchAll({
                    includeUncontrolled: false,
                });
                for (const client of allClients) {
                    client.postMessage({ev:"close"});
                }
                setTimeout(()=>{
                    wsinit(url);
                },1000);
            });
        }
        wsinit(url);
        self.addEventListener("message", (event) => {
            
            let data = event.data;
            if (typeof data !== "object" || data===null) {
                return;
            }
            if (typeof data.ev !== "string") {
                return;
            }
            if (data.ev=="send") {
                if (this.ws.readyState!==WebSocket.OPEN) {
                    event.source.postMessage({ev:"close"});
                    return;
                }
                this.ws.send(`${JSON.stringify({ path:data.data.path, id:`${event.source.id},${data.data.id}`,status:data.data.status })}\n${data.data.msg}`);
            } else if (data.ev=="isopen") {
                if (this.ws.readyState===WebSocket.OPEN) {
                    event.source.postMessage({ev:"open"});
                    return;
                }
                else {
                    event.source.postMessage({ev:"error",data:"websocket not opened"});
                }
            }
        });
    }
}