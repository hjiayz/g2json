// Copyright 2023 hjiay
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use anyhow::{Error, Result};

pub fn gen_ts(fd: &prost_types::FileDescriptorSet) -> Result<String> {
    let mut files = Vec::with_capacity(fd.file.len());
    let mut typedefs = Vec::new();
    let mut packagenames = String::new();
    let mut types = String::new();
    let mut meta_types = String::new();
    let mut meta_packages = String::new();
    for file in &fd.file {
        files.push(gen_file(file,&mut typedefs,&mut packagenames,&mut types,&mut meta_types,&mut meta_packages)?);
    }
    let typedefs = typedefs.join("");
    let result = files.join(",\n");
    let client = include_str!("client.ts");
    Ok(format!(
        r#"
{client}

{types}
type bool = boolean
type bytes = Uint8Array
type f32 = number
type f64 = number
type i32 = number
type i64 = bigint | number
type u32 = number
type u64 = bigint | number
type str = string

let types = {{
    bool : {{
        encode: (value:bool) => {{
            if (typeof value == "boolean") {{
                return value;
            }}
            throw new Error("Type mismatch not a boolean")
        }},
        decode: (value:any) => {{
            if (typeof value !== "boolean") {{
                throw new Error("Type mismatch not a boolean")
            }}
            return value;
        }},
    }},
    /**
     * byte array
     * @typedef {{Uint8Array}} bytes
     */
    bytes : {{
        encode: (value:bytes) => {{
            if (value instanceof Uint8Array) {{
                return [...value].map((i)=>i.toString(16).padStart(2,"0")).join("")
            }}
            throw new Error("Type mismatch not a dataview")
        }},
        decode: (value:any) => {{
            if (typeof value !== "string") {{
                throw new Error("Type mismatch not a string")
            }}
            return new Uint8Array((value.match(/[\da-f]{{2}}/gi)??[]).map(function (h) {{
                return parseInt(h, 16)
            }}))
        }},
    }},
    /**
     * float
     * @typedef {{number}} f32
     */
    f32 : {{
        encode: (value:f32) => {{
            if (typeof value != "number" ) {{
                throw new Error("Type mismatch not a number");
            }}
            if ((value < -3.402823E38) || (value > 3.402823E38)) {{
                throw new Error("Type mismatch not a float");
            }}
            return value;
        }},
        decode: (value:any) => {{
            if (typeof value !== "number") {{
                throw new Error("Type mismatch not a boolean")
            }}
            return value;
        }},
    }},
    /**
     * double
     * @typedef {{number}} f64
     */
    f64 : {{
        encode: (value:f64) => {{
            if (typeof value != "number" ) {{
                throw new Error("Type mismatch not a number");
            }}
            return value;
        }},
        decode: (value:any) => {{
            if (typeof value !== "number") {{
                throw new Error("Type mismatch not a boolean")
            }}
            return value;
        }},
    }},
    /**
     * unsigned 32 bit int
     * @typedef {{number}} u32
     */
    u32 : {{
        encode: (value:u32) => {{
            let v = parseInt(value.toString());
            if (( v > 4294967295 ) || ( v < 0 )) {{
                throw new Error("Type mismatch not a u32");
            }}
            return v;
        }},
        decode: (value:any) => {{
            if (typeof value !== "number") {{
                throw new Error("Type mismatch not a boolean")
            }}
            return value;
        }},
    }},
    /**
     * unsigned 64 bit int
     * @typedef {{BigInt}} u64
     */
    u64 : {{
        encode: (value:u64) => {{
            let v = BigInt(value);
            if (( v > 18446744073709551615n ) || ( v < 0n )) {{
                throw new Error("Type mismatch not a u64");
            }}
            return value.toString();
        }},
        decode: (value:any) => BigInt(value),
    }},
    /**
     * signed 32 bit int
     * @typedef {{number}} i32
     */
    i32 : {{
        encode: (value:i32) => {{
            let v = parseInt(value.toString());
            if (( v > 2147483647 ) || ( v < -2147483648 )) {{
                throw new Error("Type mismatch not a i32");
            }}
            return v;
        }},
        decode: (value:any) => {{
            if (typeof value !== "number") {{
                throw new Error("Type mismatch not a boolean")
            }}
            return value;
        }}
    }},
    /**
     * signed 64 bit int
     * @typedef {{BigInt}} i64
     */
    i64 : {{
        encode: (value:i64) => {{
            let v = BigInt(value);
            if (( v > 9223372036854775807n ) || ( v < -9223372036854775808n )) {{
                throw new Error("Type mismatch not a i64");
            }}
            return value.toString();
        }},
        decode: (value:any) => BigInt(value),
    }},
    /**
     * string
     * @typedef {{string}} str
     */
    str : {{
        encode: (value:string) => {{
            if (typeof value != "string" ) {{
                throw new Error("string encode:Type mismatch not a string");
            }}
            return value;
        }},
        decode: (value:any) => {{
            if (typeof value != "string" ) {{
                throw new Error("string decode:Type mismatch not a string");
            }}
            return value;
        }},
    }}
{typedefs}
}}

export const meta = {{
    types: {{{meta_types}
    }},
    packages: {{{meta_packages}
    }}
}}

/** remote rpc Connect. 
 */
export class Remote {{
    /**
     *
     * @callback simpleEvent
     */
    /**
     * create new Remote
     * @param {{string}} url
     * @param {{simpleEvent}} [onclose]
     */
    client : Client|SWClient;
    
    /**
     * @typedef {{Object}} Packages{packagenames}
     */
    /** @member {{Packages}} */
    packages = {{{result}
    }}
    constructor(url:string,onstatechange?:(isopen:boolean)=>void){{
        if (url==="sw") {{
            this.client = new SWClient(onstatechange);
        }}
        else {{
            this.client = new Client(url,onstatechange);
        }}
    }}
    /**
     * connect to url
     * @return {{Remote}} This Remote.
     */
    async connect() {{
        await this.client.connect();
        return this;
    }}
    /**
     * close connection
     * @return {{Remote}} This Remote.
     */
    async close() {{
        await this.client.close();
        return this;
    }}
    
}}


"#
    ))
}

fn gen_file(f: &prost_types::FileDescriptorProto,typedefs:&mut Vec<String>,packagenames: &mut String,types:&mut String,meta_types:&mut String,meta_packages:&mut String) -> Result<String> {
    let name = f.package();
    packagenames.push_str(&format!("\n         * @property {{Package_{name}}} {name}"));
    let syntax = f.syntax();
    let mut messages = Vec::with_capacity(f.message_type.len());
    for msg in &f.message_type {
        messages.push(format!(",{}",gen_message(msg, name, syntax,types,meta_types,None)?));
    }
    let messages = messages.join("\n");

    let mut enums = Vec::with_capacity(f.enum_type.len());
    for tenum in &f.enum_type {
        enums.push(format!(",{}",gen_enum(tenum,name,types,meta_types)?));
    }
    let enums = enums.join("\n");

    let mut services = Vec::with_capacity(f.service.len());
    let mut servicenames = String::new();
    let mut meta_services = String::new();
    for service in &f.service {
        services.push(gen_service(name, service,&mut servicenames,&mut meta_services)?);
    }
    let services = services.join("\n");
    typedefs.push(format!("{messages}{enums}"));
    meta_packages.push_str(&format!("\"{name}\":{{{meta_services}}},"));
    Ok(format!(
        r#"
                /**
                 * @typedef {{Object}} Package_{name}{servicenames}
                 */
                /**
                 * @member
                 */
                "{name}" : {{ {services} }}
    "#
    ))
}

fn gen_message(msg: &prost_types::DescriptorProto, package: &str, syntex: &str,types:&mut String,meta_types:&mut String,parent:Option<&str>) -> Result<String> {
    let name = msg.name();

    let fq_message_name = match parent {
        Some(s)=>format!("{s}_{}",name.replace("_", "__")),
        None => format!(
        "{}{}_{}",
        if package.is_empty() { "" } else { "_" },
        package.replace("_", "__"),
        name.replace("_", "__")),
    };
    if let Some(opt) = &msg.options {
        if opt.map_entry() {
            let key= msg.field.iter().find(|field|field.name()=="key").ok_or_else(||Error::msg("key missing"))?;
            let value= msg.field.iter().find(|field|field.name()=="value").ok_or_else(||Error::msg("value missing"))?;
            let (tkey,_) = gettype(key)?;
            let (tvalue,_) = gettype(value)?;
            types.push_str(&format!("type {fq_message_name} = [{},{}]\n",tkey,tvalue));
            meta_types.push_str(&format!(r#"
            "{fq_message_name}" : {{
                kind : "pair",
                key:"{tkey}",
                value:"{tvalue}",
            }},
            "#));
            return Ok(format!(
                r#"
            "{fq_message_name}" : {{
                    encode:(value:{fq_message_name}) => {{
                        if (!Array.isArray(value)) {{
                            throw new Error("not a map pair");
                        }}
                        if (value.length!==2) {{
                            throw new Error("not a map pair");
                        }}
                        return [types["{tkey}"].encode(value[0]),types["{tvalue}"].encode(value[1])];
                    }},
                    decode:(value) => {{
                        if (!Array.isArray(value)) {{
                            throw new Error("not a map pair");
                        }}
                        if (value.length!==2) {{
                            throw new Error("not a map pair");
                        }}
                        return [types["{tkey}"].decode(value[0]),types["{tvalue}"].decode(value[1])];
                    }},
                }}
                "#
            ));
        }
    }
    let mut nest_messages: String = String::new();
    for nest in &msg.nested_type {
        nest_messages.push_str(&gen_message(nest,package,syntex,types,meta_types,Some(&fq_message_name))?);
        nest_messages.push_str(",");
    }

    let mut fields = Vec::with_capacity(msg.field.len());
    let mut props = String::new();
    let mut tprops = String::new();
    let mut meta_props = String::new();
    for field in &msg.field {
        if field.oneof_index.is_some() {
            continue;
        }
        fields.push(gen_message_field(field, syntex,&fq_message_name,&mut props,&mut tprops,&mut meta_props)?);
    }
    gen_oneof(msg,package,syntex,Some(&fq_message_name),&mut nest_messages,types,meta_types,&mut fields,&mut tprops,&mut meta_props)?;
    tprops.pop();
    types.push_str(&format!("type {fq_message_name} = {{{tprops}}}\n"));
    meta_types.push_str(&format!(r#"
    "{fq_message_name}" : {{
        kind : "message",
        props : {{{meta_props}}}
    }},
"#));
    Ok(format!(
        r#"
{nest_messages}

    /**
     * @global
     * @typedef {{Object}} {fq_message_name}{props}
     */
    "{fq_message_name}" : {{
            encode:(value:{fq_message_name}) => {{
                return {{{}
                }};
            }},
            decode:(value) => {{
                return {{{}
                }};
            }},
        }}
        "#
        ,fields.iter().map(|v|v.0.as_str()).collect::<Vec<_>>().join(""),fields.iter().map(|v|v.1.as_str()).collect::<Vec<_>>().join("")
    ))
}

fn gen_message_field(
    field: &prost_types::FieldDescriptorProto,
    syntex: &str,
    fq_message_name:&str,
    props:&mut String,
    tprops:&mut String,
    meta_props:&mut String,
) -> Result<(String,String)> {
    use prost_types::field_descriptor_proto::{Type,Label};
    let name = field.name();
    let (mut ty,mut is_scalar) = gettype(field)?;
    match field.label() {
        Label::Optional => {
            if syntex == "proto2" || (!is_scalar) {
                props.push_str(&format!("\n         * @property {{{ty}}} [{name}]"));
                tprops.push_str(&format!("{name}? : {ty},"));
                meta_props.push_str(&format!(r#"{name} : {{
                    typeName:"{ty}",
                    kind:"option",
                }},"#));
                Ok((format!(
                    r#"
                    "{name}" : ((value:{ty}|undefined) => {{
                        if (value === undefined || value === null) {{
                            return undefined;
                        }}
                        return types["{ty}"].encode(value);
                    }})(value["{name}"]),
"#
                ),
                format!(
                    r#"
                    "{name}" : ((value:{ty}|undefined) => {{
                        if (value === undefined || value === null) {{
                            return undefined;
                        }}
                        return types["{ty}"].decode(value);
                    }})(value["{name}"]),
"#
                )
            ))
            } else {
                props.push_str(&format!("\n         * @property {{{ty}}} {name}"));
                tprops.push_str(&format!("{name} : {ty},"));
                meta_props.push_str(&format!(r#"{name} : {{
                    typeName:"{ty}",
                    kind:"required",
                }},"#));
                Ok((format!(
                    r#"
                    "{name}" : types["{ty}"].encode(value["{name}"]),
"#
                ),
                format!(
                    r#"
                    "{name}" : types["{ty}"].decode(value["{name}"]),
"#
                )
            ))
            }
        }
        Label::Required => {
            props.push_str(&format!("\n         * @property {{{ty}}} {name}"));
            tprops.push_str(&format!("{name} : {ty},"));
            meta_props.push_str(&format!(r#"{name} : {{
                typeName:"{ty}",
                kind:"required",
            }},"#));
            Ok((format!(
                r#"
                "{name}" : types["{ty}"].encode(value["{name}"]),
"#
            ),
            format!(
                r#"
                "{name}" : types["{ty}"].decode(value["{name}"]),
"#
            )
        ))
        }
        Label::Repeated => {
            props.push_str(&format!("\n         * @property {{{ty}[]}} {name}"));
            tprops.push_str(&format!("{name} : {ty}[],"));
            meta_props.push_str(&format!(r#"{name} : {{
                typeName:"{ty}",
                kind:"repeated",
            }},"#));
            Ok((format!(
                r#"
                "{name}" : value["{name}"].map(types["{ty}"].encode),
"#
            ),
            format!(
                r#"
                "{name}" : value["{name}"].map(types["{ty}"].decode),
"#
            )
        ))
        }
    }
}

fn gen_enum(tenum: &prost_types::EnumDescriptorProto, package: &str,types: &mut String,meta_types:&mut String) -> Result<String> {
    let name = tenum.name();
    let fq_message_name = format!(
        "{}{}.{}",
        if package.is_empty() { "" } else { "." },
        package,
        name
    );
    let mut values = Vec::with_capacity(tenum.value.len());
    for value in &tenum.value {
        values.push(gen_enum_value(value)?);
    }
    let values = values.join(",");
    let tvalues:Vec<String> =  tenum.value.iter().map(|item|format!("\"{}\"",item.name())).collect();
    let tvalues = tvalues.join(" | ");
    types.push_str(&format!("type {fq_message_name} = {tvalues}\n"));
    meta_types.push_str(&format!(r#"
        "{fq_message_name}" : {{
            kind : "enum",
            fields : {{{values}
            }}
        }}"#));
    Ok(format!(
        r#"
    /**
     * @global
     * @typedef {{string}} {fq_message_name}
     */
    "{fq_message_name}" : {{
        encode:(value:{fq_message_name}) => {{
            if (meta.types["{fq_message_name}"].fields[value]===undefined) {{
                throw Error("type mismatch, not a {fq_message_name}")
            }}
            return value;
        }},
        decode:(value:any) => {{
            if (meta.types["{fq_message_name}"].fields[value]===undefined) {{
                throw Error("type mismatch, not a {fq_message_name}")
            }}
            return value;
        }}
    }}
    "#
    ))
}

fn gen_enum_value(value: &prost_types::EnumValueDescriptorProto) -> Result<String> {
    let name = value.name();
    let id = value.number();

    Ok(format!(
        r#"
            {name} : {id}"#
    ))
}

fn gen_service(packagename: &str, service: &prost_types::ServiceDescriptorProto,servicenames:&mut String,meta_services:&mut String) -> Result<String> {
    let servicename = service.name();
    servicenames.push_str(&format!("\n                 * @property {{Service_{packagename}_{servicename}}} {servicename}"));
    let name = format!("{packagename}_{servicename}");
    let mut methods = Vec::with_capacity(service.method.len());
    let mut methodnames = String::new();
    let mut meta_methods = String::new();
    for method in &service.method {
        methods.push(gen_method(&name,servicename,packagename, method,&mut methodnames,&mut meta_methods)?);
    }
    meta_services.push_str(&format!("\"{name}\":{{{meta_methods}}},"));
    let methods = methods.join(",");
    Ok(format!(
        r#"
        /**
         * @global
         * @typedef {{Object}} Service_{packagename}_{servicename}{methodnames}
         */
        
        /**
         * @member
         */
        "{servicename}" : {{ {methods} }}

    "#
    ))
}

fn gen_method(name: &str,servicename:&str,packagename: &str, method: &prost_types::MethodDescriptorProto,methodnames:&mut String,meta_methods:&mut String) -> Result<String> {
    let method_name = method.name();
    methodnames.push_str(&format!("\n        * @property {{Method_{packagename}_{servicename}_{method_name}}} {method_name}"));
    let path = format!("{}{packagename}/{servicename}/{method_name}",if packagename.is_empty() {""} else {"/"} );
    let mut method_define = String::new();
    let f = match (method.server_streaming(), method.client_streaming()) {
        (true, true) => gen_bidistream(method_name,servicename,packagename, method,&path,&mut method_define,meta_methods)?,
        (false, true) => gen_upstream(method_name,servicename,packagename, method,&path,&mut method_define,meta_methods)?,
        (true, false) => gen_downstream(method_name,servicename,packagename, method,&path,&mut method_define,meta_methods)?,
        (false, false) => gen_once(method_name,servicename,packagename, method,&path,&mut method_define,meta_methods)?,
    };
    Ok(format!(
        r#"
        /**
         * @global
         * @typedef {{Object}} Method_{packagename}_{servicename}_{method_name}{method_define}
         */
        
        "{method_name}" : {f}
"#
    ))
}

fn gen_bidistream(name: &str,servicename: &str,packagename: &str, method: &prost_types::MethodDescriptorProto,path:&str,method_define:&mut String,meta_methods:&mut String) -> Result<String> {
    let input_type = method.input_type().replace("_","__").replace(".", "_");
    let output_type = method.output_type().replace("_","__").replace(".", "_");
    let sname = heck::AsSnakeCase(name);
    let sservicename = heck::AsSnakeCase(servicename);
    meta_methods.push_str(&format!(r#"
    "{name}" : {{
        kind : "bidistream",
        input : "{input_type}",
        output : "{output_type}"
    }},"#));
    Ok(format!(
        r#" (sender:Sender<{input_type}>)=>{{
            return this.client.bidistream("{path}",types["{input_type}"].encode,types["{output_type}"].decode,sender);
        }}
    
    "#
    ))
}
fn gen_upstream(name: &str,servicename: &str,packagename: &str, method: &prost_types::MethodDescriptorProto,path:&str,method_define:&mut String,meta_methods:&mut String) -> Result<String> {
    let input_type = method.input_type().replace("_","__").replace(".", "_");
    let output_type = method.output_type().replace("_","__").replace(".", "_");
    let sname = heck::AsSnakeCase(name);
    let sservicename = heck::AsSnakeCase(servicename);
    meta_methods.push_str(&format!(r#"
    "{name}" : {{
        kind : "upstream",
        input : "{input_type}",
        output : "{output_type}"
    }},"#));
    Ok(format!(
        r#" (sender:Sender<{input_type}>) => {{
                return this.client.upstream("{path}",types["{input_type}"].encode,types["{output_type}"].decode,sender);
            }}
    "#
    ))
}
fn gen_downstream(name: &str,servicename: &str,packagename: &str, method: &prost_types::MethodDescriptorProto,path:&str,method_define:&mut String,meta_methods:&mut String) -> Result<String> {
    let input_type = method.input_type().replace("_","__").replace(".", "_");
    let output_type = method.output_type().replace("_","__").replace(".", "_");
    let sname = heck::AsSnakeCase(name);
    let sservicename = heck::AsSnakeCase(servicename);
    meta_methods.push_str(&format!(r#"
    "{name}" : {{
        kind : "downstream",
        input : "{input_type}",
        output : "{output_type}"
    }},"#));
    Ok(format!(
        r#" (msg)=>{{
            return this.client.downstream("{path}",msg,types["{input_type}"].encode,types["{output_type}"].decode);
        }}
    
    "#
    ))
}
fn gen_once(name: &str,servicename: &str,packagename: &str, method: &prost_types::MethodDescriptorProto,path:&str,method_define:&mut String,meta_methods:&mut String) -> Result<String> {
    let input_type = method.input_type().replace("_","__").replace(".", "_");
    let output_type = method.output_type().replace("_","__").replace(".", "_");
    let sname = heck::AsSnakeCase(name);
    let sservicename = heck::AsSnakeCase(servicename);
    meta_methods.push_str(&format!(r#"
    "{name}" : {{
        kind : "once",
        input : "{input_type}",
        output : "{output_type}"
    }},"#));
    Ok(format!(
        r#" async(msg:{input_type})=>{{
            return await this.client.call("{path}",msg,types["{input_type}"].encode,types["{output_type}"].decode);
        }}
    
    "#
    ))
}


fn gettype(    field: &prost_types::FieldDescriptorProto)->Result<(String,bool),anyhow::Error>{
    use prost_types::field_descriptor_proto::{Type,Label};
    let name = field.name();
    let tyname;
    let mut is_scalar = true;
    let ty = match field.r#type() {
        Type::Bool => "bool",
        Type::Bytes => {
            "bytes"
        },
        Type::Float => "f32",
        Type::Double => "f64",
        Type::Fixed32 | Type::Uint32 => "u32",
        Type::Fixed64 | Type::Uint64 => "u64",
        Type::Int32 | Type::Sfixed32 | Type::Sint32 => "i32",
        Type::Int64 | Type::Sfixed64 | Type::Sint64 => "i64",
        Type::String => "str",
        Type::Enum => {
            is_scalar = false;
            tyname = field.type_name().replace("_","__").replace(".", "_");
            &tyname
        }
        Type::Message => {
            is_scalar = false;

            tyname = field.type_name().replace("_","__").replace(".", "_");
            &tyname
        }
        Type::Group => return Err(Error::msg("group is deprecated")),
    };
    Ok((ty.to_string(),is_scalar))
}

fn gen_oneof(msg: &prost_types::DescriptorProto,package:&str,syntex:&str,parent:Option<&str>,nest_messages:&mut String,types:&mut String,meta_types:&mut String,parentfields:&mut Vec<(String,String)>,parenttprops:&mut String,parentmeta_props:&mut String)->Result<()>{
    let mut oneofitems = vec![(String::new(),Vec::<(String,String)>::new(),String::new(),String::new(),String::new());msg.oneof_decl.len()];
    let id = 0;
    for oneof in &msg.oneof_decl {
        let name = oneof.name.as_ref().ok_or_else(||Error::msg("missing oneof name"))?.to_string();

        oneofitems[id].0 = name;
    }
    
    for field in &msg.field {
        if let Some(id) = field.oneof_index {
            let name = oneofitems[id as usize].0.to_string();
            let fq_message_name = match parent {
                Some(s)=>format!("{s}_{}",name.replace("_", "__")),
                None=>unreachable!(),
            };
            oneofitems[id as usize].4 = fq_message_name.clone();
            let mut props = String::new();
            let mut tprops = String::new();
            let mut meta_props = String::new();
            let fieldcode = gen_oneof_field(field, syntex,&fq_message_name,&mut props,&mut tprops,&mut meta_props)?;
            oneofitems[id as usize].1.push(fieldcode);
            oneofitems[id as usize].2.push_str(&tprops);
            oneofitems[id as usize].3.push_str(&meta_props);

        }
    }
    for (name,fields,tprops,meta_props,fq_message_name) in oneofitems {
    
        parenttprops.push_str(&format!("{name}? : {fq_message_name},"));
        parentmeta_props.push_str(&format!(r#"{name} : {{
            typeName:"{fq_message_name}",
            kind:"option",
        }},"#));
        parentfields.push((format!(
            r#"
            "{name}" : ((value:{fq_message_name}|undefined) => {{
                if (value === undefined || value === null) {{
                    return undefined;
                }}
                return types["{fq_message_name}"].encode(value);
            }})(value["{name}"]),
"#
        ),
        format!(
            r#"
            "{name}" : ((value:{fq_message_name}|undefined) => {{
                if (value === undefined || value === null) {{
                    return undefined;
                }}
                return types["{fq_message_name}"].decode(value);
            }})(value["{name}"]),
"#
        )));
        let uname = heck::AsUpperCamelCase(&name).to_string();
        types.push_str(&format!("type {fq_message_name} = {{{tprops}}}\n"));
        meta_types.push_str(&format!(r#"
        "{fq_message_name}" : {{
            kind : "message",
            props : {{{meta_props}}}
        }},
    "#));
    nest_messages.push_str(&format!(
            r#"
    
        "{fq_message_name}" : {{
                encode:(value:{fq_message_name}) => {{
                    if (Object.values(value).reduce((a,b)=>a+((b===undefined||b===null)?0:1),0)!==1) {{
                        throw new Error("encode error:not a {fq_message_name}, too many values");
                    }}
                    return {{{}
                    }};
                }},
                decode:(value) => {{
                    if (Object.values(value).reduce((a,b)=>a+((b===undefined||b===null)?0:1),0)!==1) {{
                        throw new Error("decode error:not a {fq_message_name}, too many values");
                    }}
                    return {{{}
                    }};
                }},
            }}
,
            "#
            ,fields.iter().map(|v|v.0.as_str()).collect::<Vec<_>>().join(""),fields.iter().map(|v|v.1.as_str()).collect::<Vec<_>>().join("")
        ));
    }
    Ok(())
}


fn gen_oneof_field(
    field: &prost_types::FieldDescriptorProto,
    syntex: &str,
    fq_message_name:&str,
    props:&mut String,
    tprops:&mut String,
    meta_props:&mut String,
) -> Result<(String,String)> {
    use prost_types::field_descriptor_proto::{Type,Label};
    let name = field.name();
    let (mut ty,mut is_scalar) = gettype(field)?;
    match field.label() {
        Label::Optional | Label::Required => {
                props.push_str(&format!("\n         * @property {{{ty}}} [{name}]"));
                tprops.push_str(&format!("{name}? : {ty},"));
                meta_props.push_str(&format!(r#"{name} : {{
                    typeName:"{ty}",
                    kind:"option",
                }},"#));
                Ok((format!(
                    r#"
                    "{name}" : ((value:{ty}|undefined) => {{
                        if (value === undefined || value === null) {{
                            return undefined;
                        }}
                        return types["{ty}"].encode(value);
                    }})(value["{name}"]),
"#
                ),
                format!(
                    r#"
                    "{name}" : ((value:{ty}|undefined) => {{
                        if (value === undefined || value === null) {{
                            return undefined;
                        }}
                        return types["{ty}"].decode(value);
                    }})(value["{name}"]),
"#
                )
            ))
        }
        Label::Repeated => {
            props.push_str(&format!("\n         * @property {{{ty}[]}} {name}"));
            tprops.push_str(&format!("{name} : {ty}[],"));
            meta_props.push_str(&format!(r#"{name} : {{
                typeName:"{ty}",
                kind:"repeated",
            }},"#));
            Ok((format!(
                r#"
                "{name}" : value["{name}"].map(types["{ty}"].encode),
"#
            ),
            format!(
                r#"
                "{name}" : value["{name}"].map(types["{ty}"].decode),
"#
            )
        ))
        }
    }
}